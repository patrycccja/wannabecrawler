package com.homework.crawler.http;

import java.io.IOException;

/**
 * Basic interface to download resources using HTTP protocol.
 */
public interface HTTPDownloader {

    /**
     * Send HEAD request for provided url and return response or error code.
     *
     * @param url
     * @return HTTP response code if response was returned OR error code (like "TIMEOUT") if download process failed.
     */
    String getResultCode(String url);

    /**
     * Send HEAD request for provided uri and return response code.
     *
     * @param url
     * @return HTTP response code.
     */
    String getResponseStatusCode(String url) throws IOException;

}
