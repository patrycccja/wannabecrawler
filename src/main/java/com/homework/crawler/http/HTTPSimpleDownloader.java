package com.homework.crawler.http;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple implementation of {@link com.homework.crawler.http.HTTPDownloader} using Apache HttpClient.
 */
public class HTTPSimpleDownloader implements HTTPDownloader {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final int connectionTimeout; // timeout in seconds

    /**
     * @param connectionTimeout timeout in seconds. 0 = no timeout.
     */
    public HTTPSimpleDownloader(int connectionTimeout) {
        super();
        this.connectionTimeout = connectionTimeout;
    }

    /**
     * @param url Any string. Preferably valid URL, but even NULL and empty strings are not illegal.
     * @return
     */
    @Override
    public String getResultCode(String url) {
        if (url==null || "".equals(url)) {
            return ExceptionCode.NO_URL_PROVIDED.name();
        }
        logger.trace("Sending request for URI=\"{}\"", url);
        try {
            String code = getResponseStatusCode(url);
            logger.info("{} code returned for \"{}\".", code, url);
            return code;
        } catch (Exception ex) {
            String code = getCodeFromException(ex).name();
            logger.info("{} error code returned for \"{}\". Download process failed.", code, url);
            //logger.trace("Exception occured.", ex);
            return code;
        }
    }

    @Override
    public String getResponseStatusCode(String url) throws IOException {
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectionTimeout * 60).setConnectTimeout(connectionTimeout * 60)
                .setSocketTimeout(connectionTimeout * 60).build();
        HttpClientBuilder clientBuilder = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig);
        CloseableHttpClient httpclient = clientBuilder.build();
        HttpHead req = new HttpHead(url); // throws IllegalArgumentException if the uri is invalid.
        CloseableHttpResponse httpResponse = httpclient.execute(req); // throws IllegalStateException eg. if url=="whatever"
        int code = httpResponse.getStatusLine().getStatusCode();
        httpResponse.close();
        httpclient.close();
        return Integer.toString(code);
    }

    /**
     * Create response code for different exception types.
     */
    private ExceptionCode getCodeFromException(Exception ex) {
        if (ex instanceof URISyntaxException || ex instanceof IllegalArgumentException || ex instanceof IllegalStateException) {
            // Careful with that. IllegalArgumentException and IllegalStateException can be thrown by many classes...
            return ExceptionCode.INVALID_URI;
        } if (ex instanceof SocketTimeoutException || ex instanceof ConnectTimeoutException) {
            return ExceptionCode.TIMEOUT;
        } else if (ex instanceof IOException) {
            return ExceptionCode.IO_EXCEPTION;
        }
        return ExceptionCode.UNKNOWN_EXCEPTION;
    }

    public enum ExceptionCode {
        NO_URL_PROVIDED, INVALID_URI, TIMEOUT, IO_EXCEPTION, UNKNOWN_EXCEPTION;
    }

}
