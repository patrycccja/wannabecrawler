package com.homework.crawler;

import java.io.FileNotFoundException;

import com.homework.crawler.io.InputFileQueue;
import com.homework.crawler.io.OutputFileQueue;
import com.homework.crawler.io.OutputQueue;
import com.homework.crawler.processor.CrawlerDispatcher;

/**
 * The application can be executed from the command line as follows:
 ```
 java -jar xxx.jar <input-url-file> <output-file> <worker-thread-count> <timeout-seconds>
 java -jar xxx.jar <input-url-file> <output-file> <worker-thread-count>
 java -jar xxx.jar <input-url-file> <output-file>
 java -jar xxx.jar <input-url-file>
 java -jar xxx.jar
 ```
 */
public class Crawler {
    private static final String defaultInputFilepath = "../sample_urls.txt";
    private static final String defaultOutputFilepath = "../sample_output.txt";
    private static final Integer defaultThreadCount = 3;
    private static final Integer defaultTimeoutSeconds = 3;


    public static void main (String[] args) {
        String inputPath = MainArgsParser.parseString(args, 0, "input-url-file", defaultInputFilepath);
        String outputPath = MainArgsParser.parseString(args, 1, "output-file", defaultOutputFilepath);
        Integer threadCount = MainArgsParser.parseInt(args, 2, "worker-thread-count", defaultThreadCount);
        Integer timeoutSeconds = MainArgsParser.parseInt(args, 3, "timeout-seconds", defaultTimeoutSeconds);

        if (threadCount <= 0) {
            System.out.println("Haha, very funny :) Try again, <worker-thread-count> must be greater than 0.");
            return;
        }
        if (timeoutSeconds < 0) {
            System.out.println("Nice try! <timeout-seconds> must be greater than or equal to 0.");
            return;
        }

        System.out.println("Welcome! Sit back and fasten your seat belts.");
        System.out.println("Starting the process...");
        try (InputFileQueue input = new InputFileQueue(inputPath)) {
            if (!input.hasNext()) {
                System.out.println("Except.... Have you noticed there is nothing to process? <input-url-file> is empty. Sorry, " +
                        "maybe next time.");
            } else {
                OutputQueue output = new OutputFileQueue(outputPath);
                output.reset();
                CrawlerDispatcher cd = new CrawlerDispatcher(threadCount, timeoutSeconds, input, output);
                cd.dispatch();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Except....  <input-url-file> is missing. Sorry, mission aborted.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
