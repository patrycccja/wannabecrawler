package com.homework.crawler;

/**
 * Helper class for parsing main() method arguments, ie. String[] args.
 */
public class MainArgsParser {

    static String parseString(String[] args, int i, String argName, String defaultVal) {
        if (args.length > i) {
            return args[i];
        }
        System.err.println("<" + argName + "> argument is missing. Default value will be used instead: " + defaultVal);
        return defaultVal;
    }

    static Integer parseInt(String[] args, int i, String argName, Integer defaultVal) {
        if (args.length > i) {
            try {
                return Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                System.err.println("Failed trying to parse <" + argName + "> argument:" + args[i] + ". Default value will be " +
                        "used instead: " + defaultVal);
            }
        }
        System.out.println("<" + argName + "> argument is missing. Default value will be used instead: " + defaultVal);
        return defaultVal;
    }
}