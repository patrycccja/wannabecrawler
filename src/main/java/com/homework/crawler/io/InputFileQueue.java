package com.homework.crawler.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Treats provided file as a queue and returns successive elements (lines) after each call
 * of next() method.
 */
public class InputFileQueue implements Iterator<String>, AutoCloseable {
    private final Scanner s;

    public InputFileQueue(String filePath) throws FileNotFoundException {
        s = new Scanner(new File(filePath));
    }

    public InputFileQueue(Scanner scanner) {
        this.s = scanner;
    }

    @Override
    public boolean hasNext() {
        return s.hasNext();
    }

    /**
     * Retrieves and removes the head of this queue, or returns null if this queue is empty.
     * @return next element or null
     */
    @Override
    public String next() {
        return s.hasNext() ? s.nextLine() : null;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void close() throws Exception {
        s.close();
    }
}
