package com.homework.crawler.io;

import java.io.File;
import java.io.IOException;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

/**
 *
 */
public class OutputFileQueue implements OutputQueue {
    private final File file;

    public OutputFileQueue(String filepath) throws IllegalArgumentException {
        if (filepath==null || "".equals(filepath)) {
            throw new IllegalArgumentException("Filepath is empty");
        }
        file = new File(filepath);
    }

    @Override
    public void reset() throws IOException {
        Files.write("", file, Charsets.UTF_8);
    }

    @Override
    public OutputFileQueue appendLn(String txt) throws IOException {
        Files.append(txt + "\r\n", file, Charsets.UTF_8);
        return this;
    }

    //multiple lines:
    //http://www.baeldung.com/guava-write-to-file-read-from-file

}
