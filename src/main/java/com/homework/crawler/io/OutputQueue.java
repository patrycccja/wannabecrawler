package com.homework.crawler.io;

import java.io.IOException;

public interface OutputQueue {
    void reset() throws IOException;
    OutputQueue appendLn(String txt) throws IOException;
}
