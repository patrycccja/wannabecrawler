package com.homework.crawler.processor;

import java.util.concurrent.Callable;

import com.homework.crawler.http.HTTPDownloader;
import com.homework.crawler.http.HTTPSimpleDownloader;

/**
 * Task called by a {@link com.homework.crawler.processor.CrawlerDispatcher}.
 * Returns url and corresponding result code obtained by a {@link com.homework.crawler.http.HTTPDownloader}.
 */
public class CrawlerTask implements Callable<CrawlerTask.Result> {
    Integer timeoutSeconds;
    String url;

    public CrawlerTask(Integer timeoutSeconds, String url) {
        this.timeoutSeconds = timeoutSeconds;
        this.url = url;
    }

    @Override
    public Result call() {
        HTTPDownloader downloader = new HTTPSimpleDownloader(timeoutSeconds);
        String code = downloader.getResultCode(url);
        return new Result(url, code);
    }

    public class Result {
        String url;
        String resultCode;

        public Result(String url, String resultCode) {
            this.url = url;
            this.resultCode = resultCode;
        }

        public String getUrl() {
            return url;
        }

        public String getResultCode() {
            return resultCode;
        }
    }
}
