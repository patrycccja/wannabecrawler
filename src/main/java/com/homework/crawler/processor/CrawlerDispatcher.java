package com.homework.crawler.processor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.homework.crawler.io.OutputFileQueue;
import com.homework.crawler.io.OutputQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main crawler class dispatching asynchronously {@link CrawlerTask}s, but not more than {@code threadCount} tasks at a time.
 * For each url read from the {@code input} one {@link CrawlerTask} is created. Result code returned by a task,
 * along with the url, is then written to the {@code output}. Then the dispatcher creates another task for another url and so on.
 */
public class CrawlerDispatcher {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected final Integer threadCount;
    protected final Integer timeoutSeconds;
    protected final Iterator<String> input;
    protected final OutputQueue output;
    protected  ListeningExecutorService listeningExecutorService;
    protected Map<String, CrawlerTask> tasks = new HashMap<>();


    public CrawlerDispatcher(int threadCount, int timeoutSeconds, Iterator<String> input, OutputQueue output) {
        if (input==null || output==null) {
            throw new IllegalArgumentException("Neither input nor output can be null.");
        }
        if (threadCount<=0) {
            throw new IllegalArgumentException("threadCount must be greater than 0.");
        }
        this.threadCount = threadCount;
        this.timeoutSeconds = timeoutSeconds;
        this.input = input;
        this.output = output;
    }

    public void dispatch() {
        start();
        executeDispatch();
        stop();
    }

    protected void start() {
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        listeningExecutorService = MoreExecutors.listeningDecorator(executorService);
    }

    protected void executeDispatch() {
        for (int i = 0; i < threadCount && input.hasNext(); i++) {
            runNextTask();
        }
        try {
            while (input.hasNext()) {
                synchronized (tasks) {
                    while (tasks.size() >= threadCount) {
                        tasks.wait();
                    }
                    runNextTask();
                }
            }
            synchronized (tasks) {  // wait for all tasks to complete
                while (tasks.size() > 0) {
                    tasks.wait();
                }
            }
        } catch (InterruptedException e) {
            logger.error("Some thread interrupted the current thread before or while the current thread " +
                    "was waiting for the opportunity to run next task.");
        }
    }

    protected void stop() {
        listeningExecutorService.shutdown();
    }

    /**
     * Assumption: this method is called when, and ONLY when there is next item in the queue (input.hasNext())
     * and number of threads did not exceeded allowed threadCount.
     */
    protected void runNextTask() {
        final String url = input.next();
        CrawlerTask ct = createTask(url);
        tasks.put(url, ct);

        ListenableFuture<CrawlerTask.Result> f = listeningExecutorService.submit(ct);

        Futures.addCallback(f, new FutureCallback<CrawlerTask.Result>() {
            @Override
            public void onSuccess(CrawlerTask.Result res) {
                try {
                    output.appendLn(url + "\t" + res.getResultCode());
                } catch (IOException e) {
                    logger.error("IOException when writing result to the file.", e);
                }
                tasks.remove(url);
                synchronized (tasks) {
                    tasks.notify();
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                logger.error("Exception occurred in CrawlerTask", throwable);
            }
        });
    }

    protected CrawlerTask createTask(String url) {
        return new CrawlerTask(timeoutSeconds, url);
    }

}
