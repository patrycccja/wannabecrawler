package com.homework.crawler.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.homework.crawler.io.OutputQueue;
import org.junit.Before;
import org.junit.Test;

public class CrawlerDispatcherTest {
    private List<String> urlList;
    private Iterator in;
    private MockOutputQueue out;

    @Before
    public void setUp() {
        String[] urls = {"http://www.acard.com/", "http://www.acer.com/", "http://www.activestate.com/",
                "http://www.adaptec.com/", "http://www.addonics.com/", "http://www.adobe.com/",
                "http://blogs.adobe.com/acroread/", "http://www.adobe.com/software/flash/about/", "http://www.amd.com/"};
        urlList = Arrays.asList(urls);
        in = urlList.iterator();
        out = new MockOutputQueue();
    }

    @Test(expected = IllegalArgumentException.class)
    public void threadCountExcTest() {
        new CrawlerDispatcher(0, 3, in, out);
    }

    @Test(expected = IllegalArgumentException.class)
    public void inputExcTest() {
        new CrawlerDispatcher(3, 3, null, out);
    }

    @Test(expected = IllegalArgumentException.class)
    public void outputExcTest() {
        new CrawlerDispatcher(3, 3, in, null);
    }

    @Test(timeout = 3000)
    public void outputTest() {
        CrawlerDispatcher cd = new CrawlerDispatcher(3, 3, in, out);
        cd.dispatch();

        // verify output size and format
        assertEquals(urlList.size(), out.getOutput().size());
        for (String s : out.getOutput()) {
            // line = anything, including empty string, followed by a tab and a code where code consist of [a-zA-Z_0-9] chars
            assertTrue(s.matches(".*\\t[\\w]+$"));
        }
    }

    @Test
    public void testThreadCountNotExceeded() {
        MockOutputQueue out = new MockOutputQueue();
        CrawlerDispatcher cd = new CrawlerDispatcherSpy(3, 3, in, out);
        try {
            cd.dispatch();
        } catch (IllegalStateException e) {
            fail(e.getMessage());
        }
    }



    private class MockOutputQueue implements OutputQueue {
        private List<String> output = new ArrayList<>();

        @Override
        public void reset() throws IOException {
            output.clear();
        }

        @Override
        public OutputQueue appendLn(String txt) throws IOException {
            output.add(txt);
            return this;
        }

        public List<String> getOutput() {
            return output; // yeah, not v.nice but it's only a mock
        }
    }

    private class CrawlerDispatcherSpy extends CrawlerDispatcher {

        public CrawlerDispatcherSpy(int threadCount, int timeoutSeconds, Iterator<String> input, OutputQueue output) {
            super(threadCount, timeoutSeconds, input, output);
        }

        @Override
        protected void runNextTask() {
            super.runNextTask();
            if (tasks.size() > threadCount) {
                throw new IllegalStateException("Number of threads exceeded allowed threadCount");
            }
        }
    }

}