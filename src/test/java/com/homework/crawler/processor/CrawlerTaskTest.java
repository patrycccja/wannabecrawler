package com.homework.crawler.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;

//TODO test CT with mocked HTTPDownloader
public class CrawlerTaskTest {
    int timeoutSeconds = 3;
    String url = "https://google.com/";
    CrawlerTask ct;


    @Before
    public void setUp() {
        ct = new CrawlerTask(timeoutSeconds, url);
    }

    @Test
    public void testOneTask() {
        CrawlerTask.Result r = ct.call();
        assertNotNull(r);
        assertEquals(url, r.getUrl());
        assertEquals("200", r.getResultCode());
    }

    @Test
    public void testOneSubmittedTask() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<CrawlerTask.Result> f = executorService.submit(ct);
        assertNotNull(f);
        CrawlerTask.Result r = null;
        try {
            r = f.get();
        } catch (InterruptedException | ExecutionException e) {
            fail("This method should not have thrown any exception.");
        }
        assertNotNull(r);
        assertEquals(url, r.getUrl());
        assertEquals("200", r.getResultCode());
        executorService.shutdown();
    }

    @Test
    public void testConcurrentTask() throws InterruptedException {
        String[] urls = {"http://www.acard.com/", "http://www.acer.com/", "http://www.activestate.com/",
                "http://www.adaptec.com/", "http://www.addonics.com/", "http://www.adobe.com/",
                "http://blogs.adobe.com/acroread/", "http://www.adobe.com/software/flash/about/", "http://www.amd.com/"};
        Map<String, CrawlerTask> tasks = new HashMap<>();
        String url;
        for (int i=0; i<urls.length; i++) {
            url = urls[i];
            tasks.put(url, new CrawlerTask(timeoutSeconds, url));
        }
        //3 threads, 9 tasks
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        List<Future<CrawlerTask.Result>> futures = executorService.invokeAll(tasks.values());
        assertEquals(urls.length, futures.size());

        List<CrawlerTask.Result> results = new ArrayList<>();
        // Check for exceptions
        try {
            for (Future<CrawlerTask.Result> future : futures) {
                // Throws an exception if an exception was thrown by the task.
                results.add(future.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            fail("This method should not have thrown any exception.");
        }
        // do we have results for all urls defined by the urls table?
        assertEquals(urls.length, results.size());
        for (CrawlerTask.Result res : results) {
            assertTrue(tasks.containsKey(res.getUrl()));
            tasks.remove(res.getUrl());
        }
        assertEquals(0, tasks.size());

        executorService.shutdown();
    }

}