package com.homework.crawler.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Scanner;

import org.junit.Test;

public class OutputFileQueueTest {

    @Test
    public void testAppending() throws Exception {
        String expected = "Hello world";
        String expected2 = "Lorem ipsum";
        String expected3 = "http://www.acard.com/";
        String filePath = "test_output.txt";

        OutputFileQueue out = new OutputFileQueue(filePath);
        out.reset();
        out.appendLn(expected).appendLn(expected2).appendLn(expected3);

        File file = new File(filePath);
        assertTrue(file.exists());
        Scanner s = new Scanner(file);
        assertTrue(s.hasNext());
        assertEquals(expected, s.nextLine());
        assertTrue(s.hasNext());
        assertEquals(expected2, s.nextLine());
        assertTrue(s.hasNext());
        assertEquals(expected3, s.nextLine());
        assertFalse(s.hasNext());

        file.delete();
    }

}