package com.homework.crawler.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Scanner;

import org.junit.Test;

public class InputFileQueueTest {

    @Test
    public void simpleInputTest() {
        String input = "http://www.acard.com/\n" +
                "http://www.acer.com/\n";
        Scanner s = new Scanner(input);
        InputFileQueue q = new InputFileQueue(s);
        assertNotNull(q);
        assertTrue(q.hasNext());
        assertEquals("http://www.acard.com/", q.next());
        assertTrue(q.hasNext());
        assertEquals("http://www.acer.com/", q.next());
        assertFalse(q.hasNext());
    }

}