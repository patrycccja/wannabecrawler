package com.homework.crawler.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Test;

public class HTTPSimpleDownloaderTest {

    @Test
    public void simple200Test() throws URISyntaxException {
        HTTPDownloader downloader = new HTTPSimpleDownloader(50);
        String url = "http://google.com/";
        String code = downloader.getResultCode(url);
        assertNotNull(code);
        assertTrue(code.length() > 0);
        try {
            String orygCode = downloader.getResponseStatusCode(url);
            assertNotNull(orygCode);
            assertTrue(orygCode.length() > 0);
            assertEquals(code, orygCode);
        } catch (IOException e) {
            fail("This method should not have thrown any exception. Check your Internet connection, just in case.");
        }
    }

    @Test
    public void simple404Test() throws URISyntaxException {
        HTTPDownloader downloader = new HTTPSimpleDownloader(50);
        String url = "https://www.facebook.com/404notfoundfortheurl";
        String code = downloader.getResultCode(url);
        assertNotNull(code);
        assertEquals("404", code);
        try {
            String orygCode = downloader.getResponseStatusCode(url);
            assertNotNull(orygCode);
            assertEquals("404", orygCode);
            assertEquals(code, orygCode);
        } catch (IOException e) {
            fail("This method should not have thrown any exception. Check your Internet connection, just in case.");
        }
    }

    @Test
    public void timeoutTest() throws URISyntaxException {
        HTTPDownloader downloader = new HTTPSimpleDownloader(1);
        String url = "http://10.0.0.10";
        String code = downloader.getResultCode(url);
        assertNotNull(code);
        assertEquals(HTTPSimpleDownloader.ExceptionCode.TIMEOUT.name(), code);
    }

    @Test
    public void ioExceptionTest() throws URISyntaxException {
        HTTPDownloader downloader = new HTTPSimpleDownloader(1);
        String url = "http://www.google.com:81";
        String code = downloader.getResultCode(url);
        assertNotNull(code);
        assertEquals(HTTPSimpleDownloader.ExceptionCode.IO_EXCEPTION.name(), code);
    }

    @Test
    public void noUrlTest() throws URISyntaxException {
        HTTPDownloader downloader = new HTTPSimpleDownloader(1);
        String code = downloader.getResultCode("");
        assertNotNull(code);
        assertEquals(HTTPSimpleDownloader.ExceptionCode.NO_URL_PROVIDED.name(), code);
    }

    @Test
    public void invalidUrlTest() throws URISyntaxException {
        HTTPDownloader downloader = new HTTPSimpleDownloader(1);
        String code = downloader.getResultCode("https://  google.com/");
        assertNotNull(code);
        assertEquals(HTTPSimpleDownloader.ExceptionCode.INVALID_URI.name(), code);
    }

    @Test
    public void invalidUrlTest2() throws URISyntaxException {
        HTTPDownloader downloader = new HTTPSimpleDownloader(1);
        String code = downloader.getResultCode("haha");
        assertNotNull(code);
        assertEquals(HTTPSimpleDownloader.ExceptionCode.INVALID_URI.name(), code);
    }

}