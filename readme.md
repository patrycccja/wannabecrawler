# Test web crawler

Actually this is NOT a web crawler. It's a single node, multithread, "HEAD scrapping" process working on a list of URLs. But
let's call it "crawler" anyway.

## RUN THE APP

The application can be executed from the command line as follows:

```
java -jar xxx.jar <input-url-file> <output-file> <worker-thread-count> <timeout-seconds>
java -jar xxx.jar <input-url-file> <output-file> <worker-thread-count>
java -jar xxx.jar <input-url-file> <output-file>
java -jar xxx.jar <input-url-file>
java -jar xxx.jar
```

Calling jar with no params is equivalent to:
```
java -jar xxx.jar ../sample_urls.txt ../sample_output.txt 3 3
```

## DESCRIPTION

### INPUT

File with one URL per line. Let's call it an input queue. NOTE: URLs doesn't have to be valid.

This file can be many gigabytes in size.

### OUTPUT

File with one result per line. Result = tab-separated URL and result code, where code is corresponding HTTP response
code or error message (eg. "TIMEOUT" or "INVALID_URI"). Example:

```
http://www.google.com/	200
	NO_URL_PROVIDED
http://www.joyent.com/	TIMEOUT
http://www.lacie.com/	IO_EXCEPTION
```

### HOW DOES IT WORK

There is a dispatcher thread that reads from an input queue and dispatches work to worker threads. But not more than
<worker-thread-count> at a time. When a worker thread completes, it will report the HTTP response code from the server,
or that some problem (eg. timeout) occurred. The dispatcher writes a record of the result and then assigns the thread more work.

## IMPLEMENTATION DETAILS

Note that the process isn't event driven. It's not that the crawler visits URLs, identifies all the hyperlinks in the page and
creates more tasks for those new URLs. No. It has a list of URLs and whenever one of threads returns the result,
the dispatcher runs another task.

So neither Fork and join framework nor Apache async HttpClient seem right for the job (although Apache HttpClient was used to
implement HTTP downloader). Or I didn't do my homework properly...

Instead dispatcher was implemented with ExecuteService. Which, by itself, doesn't work exactly the way we need - it's possible
to get a Future from the submitted task, but there is no method which allows to set a callback. Fortunately Google Guava offers
`ListeningExecutorService` which extends `ExecutorService`. When submitting new task it returns `ListenableFuture`. There is
also a simple utility method in `Futures` class - `addCallback()`, which adds a callback to the `ListenableFuture`. And that's
it, job done :)

NOTE: 
No interfaces were created since this is such a basic test project and won't be developed into anything bigger.